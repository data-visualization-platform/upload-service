#!/bin/bash

host=$1

if [[ -n "$host" ]]; then
	CONTAINERS=$(docker ps | grep 9092 | awk '{print $1}')
	BROKERS=$(for CONTAINER in ${CONTAINERS}; do docker port "$CONTAINER" 9092 | sed -e "s/0.0.0.0:/$host:/g"; done)
	BROKERS_IP_ADDRESSES=$(echo ${BROKERS[*]}) | tr ' ' ','
	echo $(echo ${BROKERS[*]}) | tr ' ' ','
else
    echo "no host ip address passed as an argument"
fi

# Problem will only work on same machine, as it grabs the IP Addresses of the
# Kafka brokers on the host running the setup
# How to make this entire configuration host independent?