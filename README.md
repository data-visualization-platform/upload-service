<h1> Data Visualization Platform </h1>
<hr>

# Kubernetes Setup

Install Minikube: https://minikube.sigs.k8s.io/docs/start/
Install Kubectl: https://kubernetes.io/docs/tasks/tools/install-kubectl/

Run: minikube start
Verify: minikube ip returns an IP address
Run: minikube dashboard

Run: kubectl apply -f deployment.yaml to deploy to kubernetes

Verify by going to <IP Address>:32000

# Setup

##### Set BROKERS_IP_ADDRESSES in .env file

HOST
```
sudo ./broker-ip.sh <ipaddress> 
```

E.g <ipaddress>: 172.17.0.1
Checkout kafka-middle-layer setup for more information

##### Setup

```
sudo docker-compose build
sudo docker-compose up
```

##### Run MsDataFileProcessorApplication.main()

Right Click on MsDataFileProcessorApplication and start

##### Validate Server Running
Open Chrome and navigate to: http://localhost:8080

##### Validate Kafka Integration working
http://localhost:8080/upload/source/<message>

##### Setup using Docker Registry

```
sudo docker pull registry.gitlab.com/data-visualization-platform/upload-service:latest
sudo docker run -p 8080:8080 registry.gitlab.com/data-visualization-platform/upload-service:latest
```

# Deployment Issues

Problem making:
```
sudo ./broker-ip.sh <ipaddress> 
```
part of the dpeloyment process.

https://github.com/docker/compose/issues/4081

# Test Later (Reminders)

DOCKER_HOST_IP=$(ifconfig | grep 'inet .*br' | sed -E 's/.*inet (.*) netmask.*/\1/')