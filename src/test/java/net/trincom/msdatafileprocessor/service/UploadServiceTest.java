package net.trincom.msdatafileprocessor.service;

import net.trincom.msdatafileprocessor.model.UploadData;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;


@SpringBootTest
public class UploadServiceTest {

    private final String CSV_FILE_PATH = "src/test/resources/interview-data.csv";

    @Autowired
    private UploadService sut;


    /**
     * Helper method to test test methods that use multipart uploads.
     *
     * @param file The original file.
     * @return The mock file.
     */
    private MultipartFile getMultiPartMockFile(File file) {
        byte[] content = null;
        try {
            content = Files.readAllBytes(file.toPath());
        }
        catch (final IOException e) {
            // handle exception
        }

        return new MockMultipartFile(
                file.getName(),
                file.getName(),
                "text/csv",
                content
        );
    }

    @Test
    void fileExists() {
        // Given
        File file = new File(CSV_FILE_PATH);

        // When
        String absolutePath = file.getAbsolutePath();

        // Then
        assertTrue(absolutePath.endsWith(CSV_FILE_PATH));
    }

    @Test
    void loadFileInMemoryTest() {
        // Given
        File file = new File(CSV_FILE_PATH);
        MultipartFile mockFile = getMultiPartMockFile(file);

        // When
        Set<UploadData> uploadData = sut.loadFileInMemory(mockFile);

        // Then
        assertEquals(uploadData.size(), 9);
        assertTrue(uploadData.stream().anyMatch(x -> x.getId().equals("1")));
        assertTrue(uploadData.stream().anyMatch(x -> x.getData().equals("document_0009")));
        assertEquals(uploadData.stream().filter(x -> x.getId().equals("1")).count(), 1);
    }
}
