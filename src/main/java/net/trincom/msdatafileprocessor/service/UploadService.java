package net.trincom.msdatafileprocessor.service;

import lombok.RequiredArgsConstructor;
import net.trincom.msdatafileprocessor.model.UploadData;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashSet;
import java.util.Set;


@Service
@RequiredArgsConstructor
public class UploadService {

    // TODO: column split dynamic according to CSV file
    public Set<UploadData> loadFileInMemory(MultipartFile file, Boolean containsHeader) {
        Set<UploadData> fileContent = new HashSet<>();
        BufferedReader br;

        try {
            String line;
            InputStream is = file.getInputStream();
            br = new BufferedReader(new InputStreamReader(is));
            int rowIndex = 0;
            while ((line = br.readLine()) != null) {
                if (containsHeader && rowIndex == 0) {
                    rowIndex++;
                    continue;
                }
                String[] columns = line.split(",");
                assert columns.length == 2; // remove line later see above
                fileContent.add(new UploadData(
                        columns[0].trim().replace("\"", ""),
                        columns[1].trim().replace("\"", "")
                ));
                System.out.println(line);
                rowIndex++;
            }
        } catch (IOException e) {
            System.err.println(e.getMessage());
        }

        return fileContent;
    }

    public Set<UploadData> loadFileInMemory(MultipartFile file) {
        return loadFileInMemory(file, true);
    }
}

