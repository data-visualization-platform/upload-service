package net.trincom.msdatafileprocessor.controller;


import lombok.RequiredArgsConstructor;
import net.trincom.msdatafileprocessor.model.UploadData;
import net.trincom.msdatafileprocessor.service.UploadService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.annotation.MultipartConfig;
import java.util.UUID;


@RestController
@RequestMapping("upload")
@MultipartConfig(maxFileSize = 25*1024*1024, maxRequestSize = 25*1024*1024)
@RequiredArgsConstructor
public class UploadController {

    private final long MAX_FILE_SIZE = 20971520; // 20MB
    private final UploadService service;

    @Autowired
    private KafkaTemplate<String, UploadData> kafkaTemplate;

    @Value("${tpd.topic-name}")
    private String TOPIC;

    @GetMapping("/source/{data}")
    public String notify(@PathVariable("data") final String data) {
        String hash = this.generateRandomHash();
        kafkaTemplate.send(TOPIC, new UploadData(hash, data));
        return "Successfully transmitted data with id: " + hash;
    }

    @RequestMapping(
            value = "/file",
            method = RequestMethod.POST,
            consumes = {"multipart/mixed", "multipart/form-data"},
            produces = "application/json"
    )
    public String uploadFile(@RequestPart(value = "file") MultipartFile file) {
        if(file.getSize() > MAX_FILE_SIZE) {
            return "FILE_SIZE_EXCEEDED";
        } else {
            service.loadFileInMemory(file);
        }
        return "Successfully uploaded file.";
    }

    private String generateRandomHash() {
        String uuid = UUID.randomUUID().toString();
        return uuid.replace("-", "");
    }
}
