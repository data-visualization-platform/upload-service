package net.trincom.msdatafileprocessor;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@SpringBootApplication
public class MsDataFileProcessorApplication {

	public static void main(String[] args) {
		SpringApplication.run(MsDataFileProcessorApplication.class, args);
	}

	@GetMapping("/")
	public String base() {
		return "Microservice - Data File Processor";
	}
}
