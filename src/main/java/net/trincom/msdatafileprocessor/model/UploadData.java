package net.trincom.msdatafileprocessor.model;

import lombok.Data;
import lombok.RequiredArgsConstructor;

@Data
@RequiredArgsConstructor
public class UploadData {
    private String id;
    private String data;

    public UploadData(String id, String data) {
        this.id = id;
        this.data = data;
    }
}
