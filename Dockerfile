# BUILD STAGE
FROM gradle:jdk11 as compile
USER root
RUN mkdir -p /app/src
COPY . /app/src
WORKDIR /app/src
RUN chown -R gradle /app/src
USER gradle
RUN gradle build --no-daemon
#RUN ls -LR /app/src

# RUNTIME
FROM openjdk:11-jre-slim as runtime
ARG BROKERS_IP_ADDRESSES
ARG KAFKA_ADVERTISED_HOST_NAME
ENV BROKERS_IP_ADDRESSES=$BROKERS_IP_ADDRESSES

COPY broker-ip.sh /
RUN chmod +x /broker-ip.sh && /broker-ip.sh $KAFKA_ADVERTISED_HOST_NAME

COPY --from=compile /app/src/build/libs/*.jar /app/app.jar
EXPOSE 8080
ENTRYPOINT ["java", "-Djava.security.egd=file:/dev/./urandom", "-jar", "/app/app.jar"]